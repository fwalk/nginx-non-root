#!/bin/bash

set -e

TSTAMP=${TSTAMP:-$(date "+%Y%m%d%H%M%S")}

cd nginxinc/stable/alpine

docker build -t nginxinc-stable-alpine-base:${TSTAMP} .
docker tag nginxinc-stable-alpine-base:latest
