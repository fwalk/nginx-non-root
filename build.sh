#!/bin/bash

set -e

TAG=${TAG:-$(date "+%Y%m%d%H%M%S")}

docker build -t nginx-sample1:$TAG nginx-sample1
docker tag nginx-sample1:$TAG nginx-sample1:latest

